//
//  ListViewModel.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import Combine
import Foundation

final class ListViewModel: ObservableObject {
    @Published private(set) var state = ViewState.idle
    @Published private(set) var schools = [School]()
    @Published var searchText = ""
    private let repository: RepositoryContract
    private var cancellable: AnyCancellable?
    var filteredSchools: [School] {
        guard !searchText.isEmpty else { return schools }
        return schools.filter { school in
            school.name.lowercased().contains(searchText.lowercased())
        }
    }

    init(repository: RepositoryContract) {
        self.repository = repository
    }
    
    func fetch() {
        state = .loading
        let requestType = RequestType.FetchList
        cancellable = repository.fetch(requestType)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished:
                    self?.state = .loaded
                case .failure(_):
                    self?.state = .error
                }
            }, receiveValue: { [weak self] (response: SchoolResponse) in
                self?.schools = response.schools
            })
    }
}
