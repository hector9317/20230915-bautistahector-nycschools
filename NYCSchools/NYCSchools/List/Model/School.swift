//
//  School.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import Foundation

struct SchoolResponse: Codable {
    let schools: [School]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.schools = try container.decode([School].self)
    }
}

struct School: Identifiable, Codable {
    let id: String
    let name: String
    let overview: String
    let phoneNumber: String
    let website: String
    let location: String
    let neighborhood: String
    let borough: String?
    let latitude: String?
    let longitude: String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case overview = "overview_paragraph"
        case phoneNumber = "phone_number"
        case website
        case location
        case neighborhood
        case borough
        case latitude
        case longitude
    }
}
