//
//  SchoolList.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import SwiftUI
import Foundation

struct SchoolList: View {
    @ObservedObject var viewModel: ListViewModel

     var body: some View {
         switch viewModel.state {
         case .idle:
             Color(.clear).onAppear {
                 viewModel.fetch()
             }
         case .loading:
             progressView
         case .loaded:
             NavigationView {
                 mainView.navigationBarTitleDisplayMode(.inline)
             }
             .searchable(text: $viewModel.searchText)
         case .error:
             VStack {
                 Image(Images.noData)
                 Button(SchoolListMessages.retry) {
                     viewModel.fetch()
                 }
             }
         }
     }

    var progressView: some View {
        ZStack {
            Color(.black)
                .opacity(0.7)
            ProgressView()
                .tint(.white)
        }
        .edgesIgnoringSafeArea(.all)
    }
    
    var mainView: some View {
        List(viewModel.filteredSchools) { school in
            NavigationLink {
                let repository = Repository()
                let detailViewModel = DetailViewModel(repository: repository, school: school)
                SchoolDetail(viewModel: detailViewModel)
            } label: {
                VStack(alignment: .leading) {
                    Text(school.name)
                        .font(.headline)
                    Text(school.borough ?? "NYC")
                        .font(.subheadline)
                    Text(school.neighborhood)
                        .font(.caption)
                }
                .padding(.vertical)
            }
        }
        .navigationTitle(SchoolListMessages.title)
    }
}

struct SchoolListPreview: PreviewProvider {
    static var previews: some View {
        let repository = RepositoryMock()
        let viewModel = ListViewModel(repository: repository)
        return SchoolList(viewModel: viewModel)
    }
}

