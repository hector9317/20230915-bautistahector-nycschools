//
//  SAT.swift
//  NYCSchools
//
//  Created by Hector Bautista on 16/09/23.
//

import Foundation

struct SATResponse: Codable {
    let results: [SAT]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.results = try container.decode([SAT].self)
    }
}

struct SAT: Identifiable, Codable {
    let id: String
    let schoolName: String
    let numOfTestTakers: String
    let averageReadingScore: String
    let averageMathScore: String
    let averageWritingScore: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case numOfTestTakers = "num_of_sat_test_takers"
        case averageReadingScore = "sat_critical_reading_avg_score"
        case averageMathScore = "sat_math_avg_score"
        case averageWritingScore = "sat_writing_avg_score"
    }
}
