//
//  DetailViewModel.swift
//  NYCSchools
//
//  Created by Hector Bautista on 16/09/23.
//

import Combine
import Foundation

final class DetailViewModel: ObservableObject {
    @Published private(set) var state = ViewState.idle
    @Published private(set) var results = [SAT]()
    private let repository: RepositoryContract
    private var cancellable: AnyCancellable?
    private(set) var school: School

    init(repository: RepositoryContract, school: School) {
        self.repository = repository
        self.school = school
    }
    
    func fetchSATResults() {
        state = .loading
        cancellable = repository.fetch(.SatResults(school.id))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.state = .loaded
            } receiveValue: { [weak self] (response: SATResponse) in
                self?.results = response.results
            }
    }
}
