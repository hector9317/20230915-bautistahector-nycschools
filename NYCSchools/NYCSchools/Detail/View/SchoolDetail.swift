//
//  SchoolDetail.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import Foundation
import SwiftUI

struct SchoolDetail: View {
    @ObservedObject var viewModel: DetailViewModel
    
    var body: some View {
        switch viewModel.state {
        case .idle:
            Color(.clear).onAppear {
                viewModel.fetchSATResults()
            }
        case .loading:
            progressView
        case .loaded:
            LazyVStack(alignment: .leading) {
                Text(viewModel.school.name)
                    .font(.title)
                satListResult
                    .padding(.vertical)
                Text(SchoolDetailMessages.overview + viewModel.school.overview)
                    .font(.caption)
                schoolInfo
                    .padding(.vertical)
            }
            .padding(.horizontal)
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity,
                   alignment: .topLeading)
        default:
            Color(.clear)
        }
    }
    
    var schoolInfo: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack {
                Image(systemName: "house.fill")
                Button {
                    let mapEndPoint = String(format: EndPoints.maps,
                                             viewModel.school.latitude ?? "40.6970193",
                                             viewModel.school.longitude ?? "-74.3093255")
                    guard let url = URL(string: mapEndPoint),
                          UIApplication.shared.canOpenURL(url) else { return }
                    UIApplication.shared.open(url)
                } label: {
                    Text(viewModel.school.location.split(separator: "(")[0])
                        .multilineTextAlignment(.leading)
                        .font(.subheadline)
                }
            }
            HStack {
                Image(systemName: "link")
                Button(viewModel.school.website) {
                    let website = viewModel.school.website
                    let endPoint = !website.hasPrefix("http") ? "http://\(website)" : website
                    if let url = URL(string: endPoint) {
                        UIApplication.shared.open(url)
                    }
                }
                .font(.subheadline)
            }
            HStack {
                Image(systemName: "phone")
                Button(viewModel.school.phoneNumber) {
                    if let url = URL(string: "tel://\(viewModel.school.phoneNumber)"),
                       UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                .font(.subheadline)
            }
        }
    }
    
    var satListResult: some View {
        ForEach(viewModel.results) { result in
            VStack(alignment: .leading) {
                Text("Number of test takers: \t\(result.numOfTestTakers)")
                    .font(.footnote)
                    .fontWeight(.semibold)
                Text("Reading Average Score: \t\(result.averageReadingScore)")
                    .font(.footnote)
                    .fontWeight(.semibold)
                Text("Math Average Score: \t\t\(result.averageMathScore)")
                    .font(.footnote)
                    .fontWeight(.semibold)
                Text("Writing Average Score: \t\(result.averageWritingScore)")
                    .font(.footnote)
                    .fontWeight(.semibold)
            }
        }
    }
    
    var progressView: some View {
        ZStack {
            Color(.black)
                .opacity(0.7)
            ProgressView()
                .tint(.white)
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct SchoolDetail_Previews: PreviewProvider {
    static var previews: some View {
        let repository = RepositoryMock()
        let school = School(id: "01M292",
                            name: "Clinton School Writers & Artists, M.S. 260",
                            overview: "verview",
                            phoneNumber: "123-456-789",
                            website: "https://www.theclintonschool.net",
                            location: "456 White Plains Road, Bronx NY 10473",
                            neighborhood: "Castle Hill-Clason Point",
                            borough: "BRONX",
                            latitude: "40.81504",
                            longitude: "-73.8561")
        let viewModel = DetailViewModel(repository: repository, school: school)
        SchoolDetail(viewModel: viewModel)
    }
}
