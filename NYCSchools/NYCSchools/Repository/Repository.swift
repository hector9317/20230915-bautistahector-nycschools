//
//  Repository.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import Combine
import Foundation

protocol RepositoryContract: AnyObject {
    func fetch<T: Codable>(_ request: RequestType) -> AnyPublisher<T, Error>
}

final class Repository: RepositoryContract {
    func fetch<T: Codable>(_ request: RequestType) -> AnyPublisher<T, Error> {
        let endpoint: String
        switch request {
        case .FetchList:
            endpoint = EndPoints.schoolsList
        case .SatResults(let dbn):
            endpoint = String(format: EndPoints.satResults, dbn)
        }
        guard let url = URL(string: endpoint) else {
            return Fail(error: AppErrors.invalidURL).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { _ in
                return AppErrors.invalidResponse
            }
            .eraseToAnyPublisher()
    }
}
