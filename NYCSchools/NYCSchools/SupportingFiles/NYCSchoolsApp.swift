//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            let repository = Repository()
            let viewModel = ListViewModel(repository: repository)
            SchoolList(viewModel: viewModel)
        }
    }
}
