//
//  Messages.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import Foundation

enum SchoolListMessages {
    static let title = "NYC Schools"
    static let retry = "Retry"
}

enum SchoolDetailMessages {
    static let overview = "Overview:\n"
    static let takers = "Number of test takers: \t"
    static let readingScore = "Reading Average Score: \t"
    static let mathScore = "Math Average Score: \t\t"
    static let writingScore = "Writing Average Score: \t"
}
