//
//  Utils.swift
//  NYCSchools
//
//  Created by Hector Bautista on 15/09/23.
//

import Foundation

enum ViewState {
    case idle
    case loading
    case loaded
    case error
}

enum EndPoints {
    static let schoolsList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?"
    static let satResults = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=%@"
    static let maps = "maps://?saddr=&daddr=%@,%@"
}

enum RequestType {
    case FetchList
    case SatResults(String)
}

enum AppErrors: Error {
    case invalidURL
    case invalidResponse
}

enum Images {
    static let home = "house.fill"
    static let link = "link"
    static let phone = "phone"
    static let noData = "NoData"
}
